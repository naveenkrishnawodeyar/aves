package com.test;

import com.sun.source.doctree.SeeTree;

import java.util.*;

public class SubSetOfArray
{
    static boolean subSet(Integer[] a, Integer[] b)
    {
        Set<Integer> set1 = new HashSet<>(Arrays.asList(a));
        Set<Integer> set2 = new HashSet<>(Arrays.asList(b));
            if(set1.size() != set2.size())
            {
                return false;
            }
                    for (Object obj : set1)
                    {
            // element not present in both?
                    if (!set2.contains(obj))
                        return false;
        }
        return true;
    }
    public static void main(String[] args)
    {
        Integer[] a1 = {1,2,2,3,1};
        Integer[] a2 = {1,2,3};
        System.out.println(subSet(a1,a2));
        Integer[] a3 = {1,2,3,4};
        System.out.println(subSet(a1,a3));
    }
}
