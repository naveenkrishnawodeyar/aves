package com.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StrFreq
{
    static void strFrequency(String st)
    {
        Map<Character,Integer> hm = new HashMap<>();

        for(char c: st.toCharArray())
        {
            if(hm.containsKey(c))
            {
                hm.put(c,hm.get(c)+1);
            }
            else
                hm.put(c,1);
        }
        System.out.println(hm);
    }
    public static void main(String[] args)
    {
        String str = "abcdABCDabcd";
        strFrequency(str);
    }
}
