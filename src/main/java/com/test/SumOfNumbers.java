package com.test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SumOfNumbers
{
    static void addNumbers()
    {
        List<Integer> intList = List.of(1,2,3,4,5);
        var sum = intList.stream().mapToInt(Integer::intValue).sum();
        System.out.println(sum);
    }
    public static void main(String[] args)
    {
        addNumbers();
    }
}
