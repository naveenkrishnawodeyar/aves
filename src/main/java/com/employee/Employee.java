package com.employee;

public class Employee
{
    private int eId;
    private String eName;
    private int eAge;
    private String eGender;
    private String eDepartment;
    private int eYearOfJoining;
    private double eSal;

    // default constructor
    public Employee()
    {

    }
    // parameterized constructor,
    public Employee(int eId, String eName, int eAge, String eGender, String eDepartment, int eYearOfJoining, double eSal)
    {
        this.eId = eId;
        this.eName = eName;
        this.eAge = eAge;
        this.eGender = eGender;
        this.eDepartment = eDepartment;
        this.eYearOfJoining = eYearOfJoining;
        this.eSal = eSal;
    }

    // setters and getters
    public int geteId() {
        return eId;
    }

    public void seteId(int eId) {
        this.eId = eId;
    }

    public String geteName() {
        return eName;
    }

    public void seteName(String eName) {
        this.eName = eName;
    }

    public int geteAge() {
        return eAge;
    }

    public void seteAge(int eAge) {
        this.eAge = eAge;
    }

    public String geteGender() {
        return eGender;
    }

    public void seteGender(String eGender) {
        this.eGender = eGender;
    }

    public String geteDepartment() {
        return eDepartment;
    }

    public void seteDepartment(String eDepartment) {
        this.eDepartment = eDepartment;
    }

    public int geteYearOfJoining() {
        return eYearOfJoining;
    }

    public void seteYearOfJoining(int eYearOfJoining) {
        this.eYearOfJoining = eYearOfJoining;
    }

    public double geteSal() {
        return eSal;
    }

    public void seteSal(double eSal) {
        this.eSal = eSal;
    }

    // toString()
    @Override
    public String toString() {
        return "Employee{" +
                "eId=" + eId +
                ", eName='" + eName + '\'' +
                ", eAge=" + eAge +
                ", eGender='" + eGender + '\'' +
                ", eDepartment='" + eDepartment + '\'' +
                ", eYearOfJoining=" + eYearOfJoining +
                ", eSal=" + eSal +
                '}';
    }
}
