package com.employee;

import java.util.*;
import java.util.stream.Collectors;

public class EmployeeIMPL
{
    public static void main(String[] args)
    {
        System.out.println("\n ******* Employee Management System *******");
        List<Employee> employeeList = new LinkedList<>();
        employeeList.add(new Employee(5,"Monu",22,"Female","Finance",2024,56789));
        employeeList.add(new Employee(2,"Bhaskar K",25,"Male","Production",2017,196789));
        employeeList.add(new Employee(1,"Gowri",2,"Female","Production",2023,66789));
        employeeList.add(new Employee(1,"Nandi",2,"Female","Production",2021,66789));
        employeeList.add(new Employee(4,"Lakshmi",20,"Female","Production",2020,76789));
        employeeList.add(new Employee(8,"Gani",3,"Male","Production",2021,46789));
        employeeList.add(new Employee(7,"Murali",20,"Male","Staff",2020,76789));
        employeeList.add(new Employee(12,"Ammie",24,"Female","HR",2020,25789));
        employeeList.add(new Employee(16,"Root",34,"Male","Staff",2012,39789));
        employeeList.add(new Employee(18,"Clara",25,"Female","Testing",2023,49789));
        employeeList.add(new Employee(18,"Carmel",25,"Female","Testing",2023,49789));
        employeeList.add(new Employee(20,"Bill",35,"Male","Developer",2020,89789));
        employeeList.add(new Employee(23,"Steve",44,"Male","Manager",2012,89789));
        employeeList.add(new Employee(23,"Helen",44,"FeMale","Manager",2012,89789));
        employeeList.add(new Employee(26,"Amelia",31,"Male","Developer",2015,49789));

    /**
        // 1. How many male and female employees are there in the organization?
            System.out.println("\nEmployee count by gender,");
            var collect = employeeList.stream()
                                        .collect(Collectors.groupingBy(Employee::geteGender, Collectors.counting()));
                System.out.println(collect);


        // 2. Print the name of all departments in the organization?
            System.out.println("/n Department names");
                employeeList.stream()
                            .map(emp-> emp.geteDepartment())
                            .distinct()
                            .forEach(System.out::println);

        // 3. What is the average age of male and female employees?
            System.out.println("\n Average age of employees,");
                var avgAge = employeeList.stream()
                                            .collect(Collectors.groupingBy(Employee::geteGender, Collectors.averagingDouble(Employee::geteAge)));
                    System.out.println(avgAge);

        // 4. Get the details of highest paid employee in the organization?
            System.out.println("\nHighest paid employee in the organization");
                var max = employeeList.stream().max(Comparator.comparing(Employee::geteSal));
                    System.out.println(max.get());

        // 5. Get the names of all employees who have joined after 2015?
            System.out.println("Employees joined after 2015");
                employeeList.stream()
                                .filter(emp -> emp.geteYearOfJoining() > 2015)
                                .map(emp -> emp.geteName())
                                .forEach(System.out::println);

        // 6. Count the number of employees in each department?
            System.out.println("\n Employee count in each department");
                var empCount = employeeList.stream()
                                            .collect(Collectors.groupingBy(Employee::geteDepartment, Collectors.counting()));
                    System.out.println(empCount);


        // 7. What is the average salary of each department?
            System.out.println("\n Average salary of each department,");
        for (Map.Entry<String, Double> stringDoubleEntry : employeeList.stream().collect(Collectors.groupingBy(Employee::geteDepartment, Collectors.averagingDouble(Employee::geteSal))).entrySet())
        {
            System.out.println(stringDoubleEntry);
        }


        // 8. Get the details of youngest male employee in the product development department?
            System.out.println("Youngest male employee in a particular department,");
                employeeList.stream()
                                .filter(emp -> emp.geteGender().equalsIgnoreCase("Male") || emp.geteDepartment().equalsIgnoreCase("product development"))
                                .min(Comparator.comparing(Employee::geteAge))
                                .ifPresent(System.out::println);

        // 9. Who has the most working experience in the organization?
            System.out.println("\n Senior most employee of the organization");
                employeeList.stream()
                                 .min(Comparator.comparing(Employee::geteYearOfJoining))
                                 .ifPresent(System.out::println);


        // 10. How many male and female employees are there in the sales and marketing team?
            System.out.println("\n Employees count in Marketing team,");
                var salesAndMarketing = employeeList.parallelStream()
                                                    .filter(emp -> emp.geteDepartment().equalsIgnoreCase("Production"))
                                                    .collect(Collectors.groupingBy(Employee::geteGender, Collectors.counting()));
                                System.out.println(salesAndMarketing);


        // 11. What is the average salary of male and female employees?
             System.out.println("\nAverage salary of employees,");
                var avgSalary = employeeList.parallelStream()
                                            .collect(Collectors.groupingBy(Employee::geteGender, Collectors.averagingDouble(Employee::geteSal)));
                                  System.out.println(avgSalary);

        // 12. List down the names of all employees in each department?
            System.out.println("\n Employees in each department,s");
                        Map<String,List<Employee>> empInDept= employeeList.parallelStream().collect(Collectors.groupingBy(Employee::geteDepartment));
                            for (Map.Entry<String,List<Employee>> emp : empInDept.entrySet() )
                            {
                                System.out.println("\n*************");
                                System.out.println("Employees in "+emp.getKey()+":");
                                List<Employee> empl = emp.getValue();

                                for (Employee e: empl)
                                {
                                    System.out.println(e.geteName());
                                }
                            }

     */
        // 13. What is the average salary and total salary of the whole organization?
            System.out.println("\n Organization Salary Summary");
                var sal = employeeList.parallelStream().collect(Collectors.summarizingDouble(Employee::geteSal));
                    System.out.println("\n 1.Total salary of the organization : "+sal.getSum());
                    System.out.println("\n 2.Average salary of the organization : "+sal.getAverage());
                    System.out.println("\n 3.Minimum salary of the organization : "+sal.getMin());
                    System.out.println("\n 4.Maximum salary of the organization : "+sal.getMax());
                    System.out.println("\n 5.Total employees count of the organization : "+sal.getCount());
    }
}
