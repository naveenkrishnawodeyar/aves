package com.employee;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EmployeeIMPL1
{
    public static void main(String[] args)
    {
        System.out.println("\n ******* Employee Management System *******");
        List<Employee> employeeList = new LinkedList<>();
        employeeList.add(new Employee(5,"Monu",22,"Female","Finance",2024,56789));
        employeeList.add(new Employee(2,"Bhaskar K",25,"Male","Production",2017,196789));
        employeeList.add(new Employee(1,"Gowri",2,"Female","Production",2023,66789));
        employeeList.add(new Employee(1,"Gowri",2,"Female","Production",2023,66789));
        employeeList.add(new Employee(1,"Nandi",2,"Female","Production",2021,66789));
        employeeList.add(new Employee(4,"Lakshmi",20,"Female","Production",2020,76789));
        employeeList.add(new Employee(8,"Gani",3,"Male","Production",2021,46789));
        employeeList.add(new Employee(7,"Murali",20,"Male","Staff",2020,76789));
        employeeList.add(new Employee(12,"Ammie",24,"Female","HR",2020,25789));
        employeeList.add(new Employee(16,"Root",34,"Male","Staff",2012,39789));
        employeeList.add(new Employee(18,"Clara",25,"Female","Testing",2023,49789));
        employeeList.add(new Employee(18,"Carmel",25,"Female","Testing",2023,49789));
        employeeList.add(new Employee(20,"Bill",35,"Male","Developer",2020,89789));
        employeeList.add(new Employee(23,"Steve",44,"Male","Manager",2012,89789));
        employeeList.add(new Employee(23,"Helen",44,"Female","Manager",2012,89789));
        employeeList.add(new Employee(26,"Amelia",31,"Male","Developer",2015,49789));

    /**
        // 1. How many male and female employees are there in the organization?
            System.out.println("\nEmployee count by gender,");
                Map<String,Long> genderCount =employeeList.parallelStream()
                                                            .collect(Collectors.groupingBy(Employee::geteGender,Collectors.counting()));
                    System.out.println(genderCount);

        // 2. Print the name of all departments in the organization?
            System.out.println("\n Department in the organization");
                employeeList.stream()
                                .map(Employee::geteDepartment)
                                .distinct().forEach(System.out::println);

        // 3. What is the average age of male and female employees?
            System.out.println("\n Average age of employees by age,");
                var avgAge = employeeList.parallelStream()
                                            .collect(Collectors.groupingBy(Employee::geteGender, Collectors.averagingDouble(Employee::geteSal)));
                    System.out.println(avgAge);

        // 4. Get the details of highest paid employee in the organization?
            System.out.println("\n Highest paid employee in the organization.");
                 var maxSal = employeeList.stream()
                                            .collect(Collectors.maxBy(Comparator.comparing(Employee::geteSal))).get();
                    System.out.println("\n Employee with the max salary in the organization:"+maxSal);

        // 5. Get the names of all employees who have joined after 2015?
            System.out.println("\nEmployees joined after 2015");
                    employeeList.stream().parallel()
                                        .filter(emp -> emp.geteYearOfJoining() > 2015)
                                        .map(Employee::geteName)
                                        .forEach(System.out::println);

        // 6. Count the number of employees in each department?
            System.out.println("\nEmployee count in each department,");
                    Map<String,Long> empCount = employeeList.stream().parallel()
                                                            .collect(Collectors.groupingBy(Employee::geteDepartment, Collectors.counting()));
                        System.out.println(empCount);

        // 7. What is the average salary of each department?
            System.out.println("\nAverage salary of each department");
                    employeeList.parallelStream()
                                .collect(Collectors.groupingBy(Employee::geteDepartment,Collectors.averagingDouble(Employee::geteSal)))
                                .entrySet()
                                .forEach(System.out::println);

        // 8. Get the details of youngest male employee in the product development department?
             System.out.println("\nYoungest employee in the organization");
                    employeeList.parallelStream()
                                .min(Comparator.comparing(Employee::geteAge))
                                .ifPresent(System.out::println);

        // 9.  Who has the most working experience in the organization?
             System.out.println("\n Senior most employee in the organization,");
                    employeeList.parallelStream()
                                    .min(Comparator.comparing(Employee::geteYearOfJoining))
                                    .ifPresent(System.out::println);

        // 10. How many male and female employees are there in the sales and marketing team?
            System.out.println("\nEmployee count in particular team,");
                    employeeList.parallelStream()
                                    .filter(emp -> emp.geteDepartment().equalsIgnoreCase("Developer"))
                                    .collect(Collectors.groupingBy(Employee::geteDepartment,Collectors.counting()))
                                    .entrySet()
                                    .forEach(System.out::println);
    */
        // 11.  What is the average salary of male and female employees?
            System.out.println("\n Average age of employees,");
                for (Map.Entry<String, Double> stringDoubleEntry : employeeList.parallelStream().collect(Collectors.groupingBy(Employee::geteGender, Collectors.averagingDouble(Employee::geteAge))).entrySet())
                {
                    System.out.println(stringDoubleEntry);
                }
    }
}
