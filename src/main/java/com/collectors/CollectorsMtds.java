package com.collectors;

import java.util.*;
import java.util.stream.*;

public class CollectorsMtds
{
    // 1.toList()
    static void listFromColln(List<Integer> list)
    {
        System.out.println("\n"+list+"\n");
        var collect = list.stream().map(num -> num * 2)
                .collect(Collectors.toList());
        System.out.println("\n"+collect+"\n");
    }

    // 2. toSet()
    static void setFromColln(List<Integer> intList)
    {
        System.out.println("\n"+intList+"\n");
        var collect = intList.stream().collect(Collectors.toSet());
        System.out.println("\n"+collect+"\n");
    }

    // 3. toCollection(),We can accumulate data in any specific collection as well.
    static void collnUsingStream(List<Integer> lst)
    {
        System.out.println("\n" +lst+" \n");
        var integerLinkedList = lst.stream().collect(Collectors.toCollection(LinkedList::new));
        System.out.println(integerLinkedList);
    }

    // 4. Counting elements: Counting()
    static void countInColln(List<Integer> lst)
    {
        System.out.println("\n" +lst+" \n");
        var collect = lst.parallelStream().distinct().map(num -> num / 2 == 0).collect(Collectors.counting());
        System.out.println(collect);
    }

    // 5. Finding minimum value minBy().,
    static void minValue(List<Integer> intlist)
    {
        var min = intlist.stream().collect(Collectors.minBy((n1,n2)-> n1-n2));//
        System.out.println(min.get());
    }
    public static void main(String[] args)
    {
        System.out.println("Collectors is an final class which extends Object class ");
        System.out.println("\n ********* \n");

        List<Integer> intList = Arrays.asList(2,4,6,8,10,8,6,4);
        listFromColln(intList);
        System.out.println("\n*************\n");
        setFromColln(intList);
        System.out.println("\n*************\n");
        collnUsingStream(intList);
        System.out.println("\n*************\n");
        countInColln(intList);
        System.out.println("\n*************\n");
        minValue(intList);

    }
}
