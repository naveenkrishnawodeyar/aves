package com.collectors.today;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.*;

public class SpltArray
{
    static void splitArray(int[] arr)
    {
        System.out.println("\n Original array:"+ Arrays.toString(arr));

        int[] subArray1 = Arrays.copyOf(arr,5);
        int[] subArray2 = Arrays.copyOfRange(arr,0,5);

        System.out.println("\n 1. Array using copyOf:"+Arrays.toString(subArray1));
        System.out.println("\n 2. Array using copyOfRange:"+Arrays.toString(subArray2));
    }
    public static void main(String[] args)
    {
        splitArray(new int[]{1,2,3,4});
    }
}
