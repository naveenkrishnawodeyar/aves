package com.collectors.today;

import java.util.HashMap;
import java.util.Map;

public class StrFrequency
{
    static void strFrequency(String st)
    {
        String str[] = st.split(" ");
        Map<String,Integer> mp = new HashMap<>();

        for(int i=0; i< str.length; i++)
        {
            if(mp.containsKey(i))
            {
                mp.put(str[i],mp.get(i)+1);
            }
            else
                mp.put(str[i],1);
        }
        System.out.println(mp);
    }
    public static void main(String[] args)
    {
        String str = "Hello Java World";
        strFrequency(str);
    }
}
