package com.collectors.today;

public class Palindrome
{
    static void palindromeCheck(String str)
    {
        StringBuilder sb = new StringBuilder(str);
                        sb.reverse();

                        if(str.contentEquals(sb))
                        {
                            System.out.println("Given string"+str+", is palindrome");
                        }
                        else
                            System.out.println("Given string"+str+", is not palindrome");

    }

    static void palindromeTest(String st)
    {
        String rev = "";

        for(int i=st.length()-1; i>=0; i--)
        {
            rev = rev+st.charAt(i);
        }

        if(st.equalsIgnoreCase(rev))
            System.out.println("Palindrome");
        else
            System.out.println("Not an Palindrome");

    }
    public static void main(String[] args)
    {
        System.out.println("\n Palindrome Program.,");
        palindromeCheck("String");
        palindromeTest("level");
    }
}
