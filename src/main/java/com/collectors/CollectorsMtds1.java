package com.collectors;

import java.util.*;
import java.util.stream.*;

public class CollectorsMtds1
{

    // 1. Finding minimum value minBy().,
    static void minValue(List<Integer> intlist)
    {
        var min = intlist.stream().min((n1,n2) -> n1-n2);
        System.out.println(min.get());
    }

    // 2. Finding maximum value in the list max().,
    static void maxValue(List<Integer> lst)
    {
        var max = lst.parallelStream().max(Comparator.comparing(Integer::intValue));
        System.out.println(max.get());
    }

    // 3. joining().,
    static void joinStrings()
    {
        List<String> list = List.of("One","Two","Three","Four","Five","Six","Seven","Eight");
        System.out.println(list+"\n");
        var collect = list.stream().collect(Collectors.joining());
        System.out.println(collect);
    }

    public static void main(String[] args)
    {
        System.out.println("Collectors is an final class which extends Object class.,");

        List<Integer> intList = Arrays.asList(2,4,6,8,10,8,6,4);
        System.out.println("\n*************\n");
        minValue(intList);
        System.out.println("\n*************\n");
        joinStrings();
        System.out.println("\n*************\n");

    }
}
